# Use a Ruby 2.7.4 slim image as the base
FROM ruby:2.7.4-slim

# Install dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    libpq-dev \
    nodejs \
    postgresql-client \
 && rm -rf /var/lib/apt/lists/*
RUN apt-get update && apt-get install -y git


# Set the working directory
WORKDIR app

# Copy the Gemfile and Gemfile.lock
COPY Gemfile Gemfile.lock /app/

# Install the gems
RUN gem install bundler && bundle install --jobs 20 --retry 5

# Copy the application code
COPY . .

# Set the environment variables
ENV RAILS_ENV=production \
    RAILS_SERVE_STATIC_FILES=true \
    DATABASE_URL=postgres://postgres@db/postgres \
    REDIS_URL=redis://redis:6379/0

# Compile the assets
RUN bundle exec rails assets:precompile

# Start the application server
CMD bundle exec rails server -b 0.0.0.0
